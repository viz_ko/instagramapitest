package cc.vizko.instagramtest;

import android.util.Log;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by viz on 2018. 1. 31..
 * © 2017 viz_ko.
 */
public class InstagramAPI {

    private static final String TAG = InstagramAPI.class.getSimpleName();
    private static final String INSTAGRAM_ROOT = "https://www.instagram.com";

    private final OkHttpClient client = new OkHttpClient();
    private Cookie cookie = new Cookie();

    class Cookie {
        String csrftoken = "";
        String mid = "";
        String sessionid = "";
        String ds_user_id = "";
    }

    interface ApiCallback<T> {
        void done(T t);
    }

    public void signIn(final String id, final String pw, final ApiCallback<String> callback) {
        // request sign in page first in order to get csrftoken
        requestSignInPage(new ApiCallback<Response>() {
            @Override
            public void done(Response response) {
                // parse & save csrftoken, mid
                parseSetCookieHeader(response.headers());

                // request sign in
                requestSignIn(id, pw, new ApiCallback<Response>() {
                    @Override
                    public void done(Response response) {
                        // parse & save session_id, ds_user_id
                        parseSetCookieHeader(response.headers());

                        // request personal info
                        requestPersonalInfo(id, new ApiCallback<String>() {
                            @Override
                            public void done(String userInfo) {
                                callback.done(userInfo);
                            }
                        });
                    }
                });
            }
        });
    }

    private void requestSignInPage(final ApiCallback<Response> callback) {
        Request request = new Request.Builder()
                .method("GET", null)
                .url(INSTAGRAM_ROOT)
                .headers(getCommonHeaders().build())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful())
                    throw new IOException("Failed at requestSignInPage" + response);
                callback.done(response);
            }
        });
    }

    private void requestSignIn(String id, String pw, final ApiCallback<Response> callback) {
        Request request = new Request.Builder()
                .method("POST", new FormBody.Builder(StandardCharsets.UTF_8)
                        .add("username", id)
                        .add("password", pw)
                        .build())
                .url(INSTAGRAM_ROOT + "/accounts/login/ajax/")
                .headers(getCommonHeaders().build())
                .addHeader("cookie", "csrftoken=" + cookie.csrftoken + "; mid=" + cookie.mid)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful())
                    throw new IOException("Failed at requestSignIn" + response);
                callback.done(response);
            }
        });
    }

    private void requestPersonalInfo(String id, final ApiCallback<String> callback) {
        Request request = new Request.Builder()
                .method("GET", null)
                .url(INSTAGRAM_ROOT + "/" + id + "/?__a=1")
                .headers(getCommonHeaders().build())
                .addHeader("cookie", getRequestCookie())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful())
                    throw new IOException("Failed at requestPersonalInfo" + response);
                callback.done(response.body().string());
            }
        });
    }

    public void requestLike(String postId, final ApiCallback<Void> callback) {
        Request request = new Request.Builder()
                .method("POST", RequestBody.create(null, ""))
                .url(INSTAGRAM_ROOT + "/web/likes/" + postId + "/like/")
                .headers(getCommonHeaders().build())
                .addHeader("cookie", getRequestCookie())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful())
                    throw new IOException("Failed at requestLike" + response);
                callback.done(null);
            }
        });
    }

    public void requestFollow(String userId, final ApiCallback<Void> callback) {
        Request request = new Request.Builder()
                .method("POST", RequestBody.create(null, ""))
                .url(INSTAGRAM_ROOT + "/web/friendships/" + userId + "/follow/")
                .headers(getCommonHeaders().build())
                .addHeader("cookie", getRequestCookie())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful())
                    throw new IOException("Failed at requestFollow" + response);
                callback.done(null);
            }
        });
    }

    private Headers.Builder getCommonHeaders() {
        return new Headers.Builder()
                .add("content-type", "application/x-www-form-urlencoded; charset=utf-8")
                .add("accept-encoding", "deflate")
                .add("accept-language", "ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4")
                .add("origin", INSTAGRAM_ROOT)
                .add("referer", INSTAGRAM_ROOT)
                .add("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/603.2.4 (KHTML, like Gecko) Version/10.1.1 Safari/603.2.4")
                .add("x-instagram-ajax", "1")
                .add("x-requested-with", "XMLHttpRequest")
                .add("x-csrftoken", cookie.csrftoken);
    }

    private String getRequestCookie() {
        return "csrftoken=" + cookie.csrftoken + "; mid=" + cookie.mid + "; ds_user_id=" + cookie.ds_user_id + "; sessionid=" + cookie.sessionid;
    }

    private void parseSetCookieHeader(Headers headers) {
        for (int i = 0, size = headers.size(); i < size; i++) {
            String name = headers.name(i);
            String value = headers.value(i);

            if ("set-cookie".equals(name)) {
                String csrftoken = findTargetElementInHeaderValue("csrftoken", value);
                String mid = findTargetElementInHeaderValue("mid", value);
                String sessionid = findTargetElementInHeaderValue("sessionid", value);
                String ds_user_id = findTargetElementInHeaderValue("ds_user_id", value);

                cookie.csrftoken = (csrftoken != null) ? csrftoken : cookie.csrftoken;
                cookie.mid = (mid != null) ? mid : cookie.mid;
                cookie.sessionid = (sessionid != null) ? sessionid : cookie.sessionid;
                cookie.ds_user_id = (ds_user_id != null) ? ds_user_id : cookie.ds_user_id;
            }
        }
    }

    private String findTargetElementInHeaderValue(String keyword, String headerValue) {
        if (headerValue.contains(keyword)) {
            Pattern pattern = Pattern.compile(keyword + "=(.+?);");
            Matcher matcher = pattern.matcher(headerValue);
            if (matcher.find()) {
                return matcher.group(1);
            }
        }
        return null;
    }
}
