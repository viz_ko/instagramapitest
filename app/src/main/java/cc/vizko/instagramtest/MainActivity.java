package cc.vizko.instagramtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.idText)
    EditText etId;

    @BindView(R.id.pwText)
    EditText etPw;

    @BindView(R.id.userInfoText)
    TextView tvUserInfo;

    InstagramAPI api = new InstagramAPI();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // make tvUserInfo scrollable
        tvUserInfo.setMovementMethod(new ScrollingMovementMethod());
    }

    @OnClick(R.id.signInButton)
    void signIn(View v) {
        String id = etId.getText().toString();
        String pw = etPw.getText().toString();

        api.signIn(id, pw, new InstagramAPI.ApiCallback<String>() {
            @Override
            public void done(final String userInfo) {
                Log.d(TAG, userInfo);

                // prettify
                String prettyUserInfo = "";
                try {
                    prettyUserInfo = new JSONObject(userInfo).toString(4);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String fpUserInfo = prettyUserInfo;

                // run on ui thread
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvUserInfo.setText(fpUserInfo);
                    }
                });
            }
        });
    }

    @OnClick(R.id.likeButton)
    void like(View v) {
        api.requestLike("1703914488515126916", new InstagramAPI.ApiCallback<Void>() {
            @Override
            public void done(Void aVoid) {
                // run on ui thread
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "좋아요 성공", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @OnClick(R.id.followButton)
    void follow(View v) {
        api.requestFollow("173560420", new InstagramAPI.ApiCallback<Void>() {
            @Override
            public void done(Void aVoid) {
                // run on ui thread
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "팔로우 성공", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
